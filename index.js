module.exports = (err, req, res, next) => {
  let status = err.status || 500
  if (status < 400) {
    status = 500
  }

  // Pass error object along for logging
  // see: node-logger-middleware
  res.err = err

  const error = {
    status,
    code: 'application-error',
    title: 'Application error'
  }

  if (process.env.NODE_ENV !== 'production') {
    error.meta = { stack: err.stack }
  }

  const payload = {
    errors: [ error ]
  }
  res.status(status).jsonApi(payload)

  next()
}
